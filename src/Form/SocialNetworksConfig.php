<?php
/**
 * @file
 * Contains \Drupal\drupal_helpers\Form\SocialNetworksConfig.
 */

namespace Drupal\common_social_networks\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SocialNetworksConfig extends ConfigFormBase {

  const CONFIG_NAME = 'common_social_networks.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'common_social_networks';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [$this::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this::CONFIG_NAME);

    $form['rss'] = [
      '#type' => 'checkbox',
      '#title' => 'RSS',
      '#default_value' => $config->get('rss'),
    ];

    $form['vkontakte'] = [
      '#type' => 'textfield',
      '#title' => 'Vkontakte',
      '#default_value' => $config->get('vkontakte'),
    ];

    $form['facebook'] = [
      '#type' => 'textfield',
      '#title' => 'Facebook',
      '#default_value' => $config->get('facebook'),
    ];

    $form['twitter'] = [
      '#type' => 'textfield',
      '#title' => 'Twitter',
      '#default_value' => $config->get('twitter'),
    ];

    $form['youtube'] = [
      '#type' => 'textfield',
      '#title' => 'Youtube',
      '#default_value' => $config->get('youtube'),
    ];

    $form['instagram'] = [
      '#type' => 'textfield',
      '#title' => 'Instagram',
      '#default_value' => $config->get('instagram'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Send form.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = &$form_state->getValues();

    $this->config($this::CONFIG_NAME)
      ->set('rss', $values['rss'])
      ->set('vkontakte', $values['vkontakte'])
      ->set('facebook', $values['facebook'])
      ->set('twitter', $values['twitter'])
      ->set('youtube', $values['youtube'])
      ->set('instagram', $values['instagram'])
      ->save();
  }

}
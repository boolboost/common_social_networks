<?php

/**
 * @file
 * Contains \Drupal\common_social_networks\Service\Rss.
 */

namespace Drupal\common_social_networks\Service;

use Drupal\views\Views;

class Rss {

  /**
   * {@inheritdoc}
   */
  public function getFeeds() {
    $feed_icons = NULL;

    if ($view_id = \Drupal::routeMatch()->getParameter('view_id')) {
      $current_display = \Drupal::routeMatch()->getParameter('display_id');

      $view = Views::getView($view_id);
      $arguments = \Drupal::routeMatch()->getRawParameters()->all();

      $displays = $view->storage->get('display');

      foreach ($displays as $display_id => &$display) {
        if ($display['display_plugin'] == 'feed') {
          $display_options = &$display['display_options'];

          if (isset($display_options['displays']) && $display_options['displays'][$current_display] == $current_display) {
            $view->setArguments($arguments);
            $view->execute($current_display);

            $feed_icons = $view->feedIcons;
            break;
          }
        }
      }
    }

    return $feed_icons;
  }
}
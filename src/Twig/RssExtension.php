<?php

namespace Drupal\common_social_networks\Twig;

use Drupal\common_social_networks\Service\Rss;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RssExtension extends AbstractExtension {

  protected $service;

  /**
   * Constructor
   *
   * @param \Drupal\common_social_networks\Service\Rss $service
   */
  public function __construct(Rss $service) {
    $this->service = $service;
  }

  /**
   * Get extension twig function
   *
   * @return \Twig\TwigFunction[]
   */
  public function getFunctions(): array {
    return [
      new TwigFunction('common_social_networks_feeds', [$this, 'getFeeds']),
    ];
  }

  /**
   * Get Feeds
   */
  public function getFeeds(): bool {
    return $this->service->getFeeds();
  }

}

### Twig template socials

``` twig
<div class="{{ bem_block }}__socials">
  {% set socials = {
    'vkontakte': {
      'href': drupal_config('common_social_networks.settings', 'vkontakte'),
      'icon': 'vk',
    },
    'instagram': {
      'href': drupal_config('common_social_networks.settings', 'instagram'),
      'icon': 'instagram',
    },
    'facebook': {
      'href': drupal_config('common_social_networks.settings', 'facebook'),
      'icon': 'facebook',
    },
    'twitter': {
      'href': drupal_config('common_social_networks.settings', 'twitter'),
      'icon': 'twitter',
    },
    'youtube': {
      'href': drupal_config('common_social_networks.settings', 'youtube'),
      'icon': 'youtube',
    },
  } %}
    
  {% for social in socials %}
    {% if social.href %}
      <a href="{{ social.href }}" class="{{ bem_block }}__socials-item">
        <i class="fa fa-{{ social.icon }}"></i>
      </a>
    {% endif %}
  {% endfor %}
</div>
```
